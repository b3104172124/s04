package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.coyote.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Retrieve all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Retrieve all posts
    @RequestMapping(value = "/myposts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken) {
        Iterable<Post> posts = postService.getMyPosts(stringToken);

        List<Object> formattedPosts = new ArrayList<>();
        for (Post post : posts) {
            formattedPosts.add(formatPost(post));
        }
        return new ResponseEntity<>(formattedPosts, HttpStatus.OK);
    }

    // Update a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        return postService.updatePost(postid, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(postid, stringToken);

    }

    private Object formatPost(Post post) {
        // Extract the required information from the post and format it as desired
        String title = post.getTitle();
        String content = post.getContent();
        User user = post.getUser();
        Long userId = user.getId();
        String username = user.getUsername();
        String password = user.getPassword();


        return Map.of("title", title, "content", content, "user", Map.of("id", userId, "username", username, "password", password));

    }

    // GET - Retrieving
    // POST - Insertion
    // PUT & PATCH - Updating
    // DELETE - Deleting


}

